This repositoy contains information on the 'High-Frequency Electronics' course. 

PLEASE KEEP THIS REPOSITORY UPDATED FOR FUTURE ASSISTENTS!

This course used be split into two seperate courses and each course was accompanied by a practical part (a power amplifier design and an antenna design).
Now, these two courses are combined into one course, and the students only need to design a power amplifier.

This repositoy contains:

- the theory course (TheoryBookUHF_EN_full.pdf)

- a folder 'exams' containing exam questions for the oral exam of the practical assignment

- a folder 'assigments' containing excelfiles that keep track of the assigments given to students

- a folder 'antenna-design' containing

	+ the datasheets to give to the students
	
	+ the literatureReferences to give to the students
	
	+ the ADS workspace for the initial antenna design (antenna_wrk)
	
	+ the compressed ADS workspace for the initial antenna design (antenna_wrk.7zads) to give to the students 
	
	+ the ADS workspace containing antenna examples (Antenna_examples_wrk)
	
	+ the assignment documentation file (HFEproject_Antenna_v2018.pdf) to give to the students
	
	+ the source files (folder figures and lyx-file)
	
- a folder 'amplifier-design' containing

	+ the datasheets to give to the students
	
	+ the literatureReferences to give to the students
	
	+ a folder with examples circuits
	
	+ the ADS workspace for the initial design to give to the students (amplifier_wrk)
		
	+ the assignment documentation file (HFEproject_Amplifier_v2019.pdf) to give to the students
	
	+ the source files (folder figures and lyx-file)